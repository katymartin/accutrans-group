/*
 * Bones Scripts File
 * Author: Eddie Machado
 *
 * This file should contain any js scripts you want to add to the site.
 * Instead of calling it in the header or throwing it inside wp_head()
 * this file will be called automatically in the footer so as not to
 * slow the page load.
 *
 * There are a lot of example functions and tools in here. If you don't
 * need any of it, just remove it. They are meant to be helpers and are
 * not required. It's your world baby, you can do whatever you want.
*/


$('.magnific').magnificPopup({
  type:'image',
  removalDelay: 300,
  mainClass: 'mfp-fade'
});
  





$("p").each(function() {
  var wordArray = $(this).text().split(" ");
  if (wordArray.length > 1) {
    wordArray[wordArray.length-2] += "&nbsp;" + wordArray[wordArray.length-1];
    wordArray.pop();
    $(this).html(wordArray.join(" "));
  }
});





	if ('ontouchstart' in window) {
	  var click = 'touchstart';
	} else {
	  var click = 'click';
	}

	$('div.bars').on(click, function() {

	  if (!$(this).hasClass('open')) {
	    openMenu();
	  } else {
	    closeMenu();
	  }

	});



	function openMenu() {

	  $('div.circle').addClass('expand');
	  $('div.circle').addClass('show');
	  $('div.menu').addClass('show');	

	  $('div.bars').addClass('open');
	  $('div.x, div.y, div.z').addClass('collapse');
	  $('.menu li').addClass('animate');

	  setTimeout(function() {
	    $('div.y').hide();
	    $('div.x').addClass('rotate30');
	    $('div.z').addClass('rotate150');
	  }, 70);
	  setTimeout(function() {
	    $('div.x').addClass('rotate45');
	    $('div.z').addClass('rotate135');
	  }, 120);

	}

	function closeMenu() {

	  $('div.bars').removeClass('open');
	  $('div.x').removeClass('rotate45').addClass('rotate30');
	  $('div.z').removeClass('rotate135').addClass('rotate150');
	  $('div.circle').removeClass('expand');
	  $('div.circle').removeClass('show');
		$('div.menu').removeClass('show');
	  $('.menu li').removeClass('animate');

	  setTimeout(function() {
	    $('div.x').removeClass('rotate30');
	    $('div.z').removeClass('rotate150');
	  }, 50);
	  setTimeout(function() {
	    $('div.y').show();
	    $('div.x, div.y, div.z').removeClass('collapse');
	  }, 70);

	}




!function(e,n){"function"==typeof define&&define.amd?define(["exports"],n):n("undefined"!=typeof exports?exports:e.dragscroll={})}(this,function(e){var n,t,o=window,l=document,c="mousemove",r="mouseup",i="mousedown",m="EventListener",d="add"+m,s="remove"+m,f=[],u=function(e,m){for(e=0;e<f.length;)m=f[e++],m=m.container||m,m[s](i,m.md,0),o[s](r,m.mu,0),o[s](c,m.mm,0);for(f=[].slice.call(l.getElementsByClassName("dragscroll")),e=0;e<f.length;)!function(e,m,s,f,u,a){(a=e.container||e)[d](i,a.md=function(n){e.hasAttribute("nochilddrag")&&l.elementFromPoint(n.pageX,n.pageY)!=a||(f=1,m=n.clientX,s=n.clientY,n.preventDefault())},0),o[d](r,a.mu=function(){f=0},0),o[d](c,a.mm=function(o){f&&((u=e.scroller||e).scrollLeft-=n=-m+(m=o.clientX),u.scrollTop-=t=-s+(s=o.clientY),e==l.body&&((u=l.documentElement).scrollLeft-=n,u.scrollTop-=t))},0)}(f[e++])};"complete"==l.readyState?u():o[d]("load",u,0),e.reset=u});


$(function() {
    if (Modernizr.touch) {
        $(".timeline").css("overflow-x", "auto");
    }
});



/**

 $(document).ready(function () {
     $("button").click(function () {
         $(".pop").fadeIn(300);
         positionPopup();
     });

     $(".pop > span").click(function () {
         $(".pop").fadeOut(300);
     });
 });

**/


// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus(2);
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });







/*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
*/
function updateViewportDimensions() {
	var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],x=w.innerWidth||e.clientWidth||g.clientWidth,y=w.innerHeight||e.clientHeight||g.clientHeight;
	return { width:x,height:y };
}
// setting the viewport width
var viewport = updateViewportDimensions();

function loadGravatars() {
  // set the viewport using the function above
  viewport = updateViewportDimensions();
  // if the viewport is tablet or larger, we load in the gravatars
  if (viewport.width >= 768) {
  jQuery('.comment img[data-gravatar]').each(function(){
    jQuery(this).attr('src',jQuery(this).attr('data-gravatar'));
  });
	}
} // end function


/*
 * Put all your regular jQuery in here.
*/
jQuery(document).ready(function($) {

  /*
   * Let's fire off the gravatar function
   * You can remove this if you don't need it
  */
  loadGravatars();


}); /* end of as page load scripts */
