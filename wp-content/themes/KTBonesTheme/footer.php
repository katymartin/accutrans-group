			<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

				<div id="inner-footer" class="wrap cf">


				</div>

  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.0/jquery.magnific-popup.min.js'></script>
			
		<section class="contactFoot clearfix" >

<div class="contactContainer">

	<div class="contact clearfix">
		
		
		<div class="conS">
		<h1 id="orange">Contact</h1>
			<a href="tel:14142262100" target="_blank"><p><i class="fa fa-phone" aria-hidden="true"></i>414.226.2100</p></a>
			
			<a href="tel:8668998895" target="_blank"><p><i class="fa fa-phone" aria-hidden="true"></i>866.899.8895</p></a>
			
			<a href="mailto:info@accutransmke.com"><p><i class="fa fa-envelope" aria-hidden="true"></i>info@accutransgroup.com</p></a>
			
			<a href="https://www.google.com/maps/place/Accutrans,+Inc./@42.9329758,-87.9116922,17z/data=!3m1!4b1!4m5!3m4!1s0x8805199641daf309:0x3494d00915b6a95d!8m2!3d42.9329719!4d-87.9094982" target="_blank"><p><i class="fa fa-map-marker" aria-hidden="true"></i>6134&nbsp;South&nbsp;Howell&nbsp;Avenue</p>
			<p style="padding-left: 20px; margin-top: -15px;">Milwaukee, WI 53207</p></a>
			
			</a>
			
			<div class="pLine">
			<p>Need a quote? <br/>
			<p>Have a general question? <br/>
			<p>We're here to help!</p><br/>
			</div>
			<div><span style="color: white; letter-spacing: 1px; hyphens: none; --max-size: 14.5; --min-size: 13; font-size: var(--responsive);">If you want to get straight to booking, give us a call or head to our</span> <a id="orange" href="https://www.mytripcenter.com/Default.aspx?sys=D215639C-BF0C-4F35-8688-1604A8934751" target="_blank">online reservation portal</a></div>
			
		</div>
		
		
			<div class="conS">
			<?php echo do_shortcode('[ninja_form id=1]'); ?>
			</div>
			
			
			
		</div>
		
	</div>

	<img src="/wp-content/themes/KTBonesTheme/library/images/atgThumb@2x.png"/>
</section>							
				


			</footer>

		</div>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>
				
<!-- start number replacer -->
<script type="text/javascript"><!--
vs_account_id      = "Ch4Nmlmu6c5YzgBP";
//--></script>
<script type="text/javascript" src="https://rw1.calls.net/euinc/number-changer.js">
</script>
<!-- end ad widget -->
	

	</body>

</html> <!-- end of site. what a ride! -->
