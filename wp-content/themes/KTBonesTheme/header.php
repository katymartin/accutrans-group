<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->



<head>
		<title><?php wp_title(''); ?></title>
	 	<?php wp_head(); ?>

	<meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta http-equiv="content-language" content="nl">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">


<link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>
<link href="/wp-content/themes/KTBonesTheme/library/js/scripts.js" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">


<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '120600308587541'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=120600308587541&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->


 <script>

 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-39015905-28', 'auto');
 ga('create', 'UA-78694691-47', 'auto', 'ppcTracker');

 ga('send', 'pageview');
 ga('ppcTracker.send', 'pageview');

</script>


</head>


<section class="deskNav">
<a href="/" target="_self"><img class="deskLogo" src="/wp-content/themes/KTBonesTheme/library/images/AccuLogo@2x.png"></a>

<ul class="menu">
		<li><a href="/services/" target="_self">Services</a></li>
		<li><a href="/fleet/" target="_self">Fleet</a></li>
		<li><a href="/about/" target="_self">About</a></li>
		<li class="bookNav"><a class="book" href="https://www.mytripcenter.com/Default.aspx?sys=D215639C-BF0C-4F35-8688-1604A8934751" target="_blank" onclick="ga('send', 'event', 'BookARide', 'Click');ga('ppcTracker.send', 'event', 'BookARide', 'Click');
		">Book a Ride</a></li>
		<li class="iconNav">
			<a href="tel:1-414-226-2100">
				<i class="fa fa-phone" aria-hidden="true"></i>
			</a>
			<a href="mailto:info@accutransgroup.com">
				<i class="fa fa-envelope" aria-hidden="true"></i>
			</a>
		</li>

	</ul>
</section>



<nav class="mobileNav">
   <a href="/" target="_self"><img class="mobileLogo" src="/wp-content/themes/KTBonesTheme/library/images/AccuLogo@2x.png"></a>
  <div class="navbar"></div>
  <div class="circle"></div>
  <div class="menu">
    <ul>
        <li><a href="/services/" target="_self">Services</a></li>
		<li><a href="/fleet/" target="_self" >Fleet</a></li>
		<li><a href="/about/" target="_self">About</a></li>
        <li><a href="https://www.mytripcenter.com/Default.aspx?sys=D215639C-BF0C-4F35-8688-1604A8934751" target="_blank"
onclick="ga('send', 'event', 'BookARide', 'Click');ga('ppcTracker.send', 'event', 'BookARide', 'Click');
">Book a Ride</a></li>
        <li class="mobileIcon">
			<a href="tel:1-414-226-2100">
				<i class="fa fa-phone" aria-hidden="true"></i>
			</a>
			<a href="mailto:info@accutransgroup.com">
				<i class="fa fa-envelope" aria-hidden="true"></i>
			</a>
		</li>
    </ul>
  </div>
  <div class="bars">
    <div class="x"></div>
    <div class="y"></div>
    <div class="z"></div>
  </div>
</div>
</nav>
