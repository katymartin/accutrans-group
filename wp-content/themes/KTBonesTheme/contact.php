<?php
/**
 Template Name: Contact
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>
<head>
	<title>Not a Meta Tag, but required anyway </title>
	<meta name="description" content="Awesome Description Here">
</head>

<body>

<section class="homeHead">
 	<div class="homeHeadContent">
		<img src="/wp-content/themes/BonesTheme/library/images/logo.png" alt="company logo">
		<div class="bannerCopy" alt=""></div>
		<div class="ctaButton">
			<a href="" target="_self"><h4>CTA</h4></a>
		</div>
	</div>
</section>


<section class="threeSec">
	<div class="threeDiv">
	</div>
	<div class="threeDiv">
	</div>
	<div class="threeDiv">
	</div>
</section>

<section class="twoSec">
	<div class="twoDiv">
	</div>
	<div class="twoDiv">
	</div>
</section>



</body>
			

<?php get_footer(); ?>
