<?php
/**
 Template Name: Services
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>
<head>
	<title>Services | AccuTrans Group</title>
	<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">

	<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
</head>

<body>





<section class="serviceBanner">
	<div class="flexSec">







			<div class="flexCont">
				<div class="flexDiv serviceImg" style="background: url('<?php the_field('services_img'); ?>'); background-repeat: no-repeat;
				background-size: cover; background-position: center center; width: 100%;"></div>

				<div class="flexDiv serviceCopy">

				<h2><?php the_field('services_title'); ?></h2>
				<div><?php the_field('services_copy'); ?></div>

				</div>
			</div>

	</div>

	<div class="fourSec serviceOpt">


			<div class="fourCont">


					<div class="servicesCat fourDiv">
					<div>
						<img  class="sIcons" src="/wp-content/themes/KTBonesTheme/library/images/conventionIcon@2x.png">
					</div>
					<div class="sCat">
					<h2><?php the_field('services_subcat1'); ?></h2>
					<p><?php the_field('services_subcatcopy1'); ?></p>
					</div>
					</div>

					<div class="servicesCat fourDiv">
					<div >
						<img  class="sIcons" src="/wp-content/themes/KTBonesTheme/library/images/groupTransportIcon@2x.png">
						</div>
					<div class="sCat">
					<h2><?php the_field('services_subcat2'); ?></h2>
					<p><?php the_field('services_subcatcopy2'); ?></p>
					</div>
					</div>

					<div class="servicesCat fourDiv">
					<div >
						<img class="sIcons" src="/wp-content/themes/KTBonesTheme/library/images/point2pointIcon@2x.png">
						</div>
					<div class="sCat">
					<h2><?php the_field('services_subcat3'); ?></h2>
					<p><?php the_field('services_subcatcopy3'); ?></p>

					</div>
					</div>

					<div class="servicesCat fourDiv">
					<div>
						<img class="sIcons" src="/wp-content/themes/KTBonesTheme/library/images/hourlyServiceIcon.png">
						</div>
					<div class="sCat">
					<h2><?php the_field('services_subcat4'); ?></h2>
					<p><?php the_field('services_subcatcopy4'); ?></p>
					</div>
					</div>


			</div>

	</div>

</div>
</section>

<!--
<section class="twoSec">
	<div class="twoCont">
		<div class="twoDiv"></div>
		<div class="twoDiv"></div>
	</div>
</section>

-->


</body>


<?php get_footer(); ?>
