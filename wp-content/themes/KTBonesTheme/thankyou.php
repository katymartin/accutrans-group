<?php
/*
 Template Name: Thank You
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('track', 'CompleteRegistration');
</script>

<head>

</head>

<body>

<section class="thankYou">
	<h1>Thank You!</h1>
	<h3>We'll be in touch soon!</h3>
	<a href="/"><p>Back to Homepage > </p></a>
</section>



	<!-- start number replacer -->
<script type="text/javascript"><!--
vs_account_id      = "Ch4Nm1mIz8kaIQAR";
//--></script>
<script type="text/javascript" src="https://rw1.calls.net/euinc/number-changer.js">
</script>
<!-- end ad widget -->

	</body>
