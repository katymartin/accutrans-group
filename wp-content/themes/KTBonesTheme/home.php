<?php
/**
 Template Name: Home
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>
<head>
	<title>AccuTrans Group</title>
	<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
	<link href="//cdn-images.mailchimp.com/embedcode/slim-10_7.css" rel="stylesheet" type="text/css">
	<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>



</head>

<body>





<section class="homeHead">
	<div class="backImg" style="background: url('<?php the_field('banner_img'); ?>'); background-repeat: no-repeat; background-size: cover; background-position: center center; width: 100%; "></div>
 	<div class="wrapper bannerCopy">
		<div>
			<h1 id="white"><?php the_field('feature_title'); ?></h1>
			<h3 id="white" ><?php the_field('feature_pg'); ?></h3>
		</div>
	<div class="greenButton">
			<a href="<?php the_field('feature_link'); ?>" target="_blank" onclick="ga('send', 'event', 'BookARide', 'Click');ga('ppcTracker.send', 'event', 'BookARide', 'Click');"><p id="white" ><?php the_field('feature_cta'); ?></h3></p></a>
</div>





	</div>
	<div class="quoteBox" id="white">
		<div class="qSec">
		<p id="white">Need a quote?</p>
		<p id="white">Have a general question?</p>
		<p id="white">We're here to help!</p>
		</div>

	<div>
  			<a href="#inner-footer">
				<button class="greenButton">
				<p style="line-height: 0px;">Contact</p>
				</button>
  			</a>
	</div>

	</div>
</section>



<section class="flexSec mountain">
<div class="fourIcons">
	<div class="fourIcoFlex">
			<div class="flexDiv homeIco">
			<img src="/wp-content/themes/KTBonesTheme/library/images/veteranOwned@2x.png"/>
			<h2>Veteran Owned</h2>
			</div>

			<div class="flexDiv homeIco">
			<img src="/wp-content/themes/KTBonesTheme/library/images/proffessional@2x.png"/>
			<h2>Professional</h2>
			</div>

			<div class="flexDiv homeIco">
			<img src="/wp-content/themes/KTBonesTheme/library/images/personal@2x.png"/>
			<h2>Personal</h2>
			</div>

			<div class="flexDiv homeIco">
			<img src="/wp-content/themes/KTBonesTheme/library/images/dependable@2x.png"/>
			<h2>Dependable</h2>
			</div>
	</div>
</div>


<div class="trees">
	<div class="steps">
		<div class="wrapper">

	<h3 id="grey"><?php the_field('homesec1_header'); ?></h3>
	<div class="flexCont">
		<div class="flexDiv">
			<div class="stepA">
			<img src="/wp-content/themes/KTBonesTheme/library/images/check@2x.png">
			</div>
			<div class="stepB">
			<h3 id="grey"><?php the_field('homesec1_subhead1'); ?></h3>
			<p id="grey"><?php the_field('homesec1_copy1'); ?></p>
		</div>
		</div>


	<div class="flexDiv">
			<div class="stepA">
			<img src="/wp-content/themes/KTBonesTheme/library/images/check@2x.png">
			</div>
			<div class="stepB">
			<h3 id="grey"><?php the_field('homesec1_subhead2'); ?></h3>
			<p id="grey"><?php the_field('homesec1_copy2'); ?></p>
		</div>
		</div>

		<div class="flexDiv">
			<div class="stepA">
			<img src="/wp-content/themes/KTBonesTheme/library/images/check@2x.png">
			</div>
			<div class="stepB">
			<h3 id="grey"><?php the_field('homesec1_subhead3'); ?> </h3>
			<p id="grey"><?php the_field('homesec1_copy3'); ?></p>
		</div>
		</div>

	</div>
		<div class="greenButton">
			<a href="https://www.mytripcenter.com/Default.aspx?sys=D215639C-BF0C-4F35-8688-1604A8934751" target="_blank" onclick="ga('send', 'event', 'BookARide', 'Click');ga('ppcTracker.send', 'event', 'BookARide', 'Click');"><p id="white" >Book a Ride</h3></p></a>
		</div>
		<img class="car"src="/wp-content/themes/KTBonesTheme/library/images/car@2x.png">
		<hr>
		</div>
	</div>
  </div>
</section>

<section class="halfBlock">
	<div class="wrapper flexCont">
		<div class="historyTeaser flexDiv">
		<h3 id="white"><?php the_field('homesec2_header'); ?></h3>
		<div style="color:white;"><?php the_field('homesec2_copy'); ?></div>
		<div class="whiteButton">
				<a href="<?php the_field('homesec2_btn_url'); ?> " target="_self"><p id="green" ><?php the_field('homesec2_btn_cta'); ?></p></a>
			</div>
		</div>
		<div class="chimp flexDiv">
		<i class="fa fa-envelope-o" aria-hidden="true"></i>
		<p>Sign up for our email newsletter to receive company updates, news, and more.</p>
		<!-- Begin MailChimp Signup Form -->

				<div id="mc_embed_signup">
				<form action="//accutransgroup.us16.list-manage.com/subscribe/post?u=3d397447feb3134d7ae6fb2d2&amp;id=da3742a7de" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
					<div id="mc_embed_signup_scroll">

					<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
					<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_3d397447feb3134d7ae6fb2d2_da3742a7de" tabindex="-1" value=""></div>
					<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="whiteButton"></div>
					</div>
				</form>
				</div>

<!--End mc_embed_signup-->
		</div>
	</div>


</section>


<section class="homeService">
<div class="wrapper">
	<div class="flexSec cars">


			<div class="flexCont">
				<div class="flexDiv">
					<img src="/wp-content/themes/KTBonesTheme/library/images/AccuFleet_01.png" />
					<div class="carName">
						<h2>Sedan</h2>
					</div>
				</div>

				<div class="flexDiv cars">
					<img src="/wp-content/themes/KTBonesTheme/library/images/AccuFleet_02.png" />
					<div class="carName">
						<h2>PREMIUM SEDAN</h2>
					</div>
				</div>

				<div class="flexDiv cars">
					<img src="/wp-content/themes/KTBonesTheme/library/images/AccuFleet_03.png" />
					<div class="carName">
						<h2>SUV</h2>
					</div>
				</div>
			</div>

	</div>
	<div class="greenButton">
			<a href="/fleet/" target="_self"><p id="white" >See Full Fleet</h3></p></a>
		</div>
	<hr>
	<div class="flexSec">


			<div class="flexCont">
				<div class="flexDiv sCat1">
					<div class="sIcons">
						<img src="/wp-content/themes/KTBonesTheme/library/images/conventionIcon@2x.png">
						<h5>ONGOING CORPORATE TRANSPORTATION</h5>
					</div>
					<div class="sIcons">
						<img src="/wp-content/themes/KTBonesTheme/library/images/groupTransportIcon@2x.png">
						<h5>CONVENTION AND GROUP SERVICES</h5>
					</div>
					<div class="sIcons">
						<img src="/wp-content/themes/KTBonesTheme/library/images/point2pointIcon@2x.png">
						<h5>AIRPORT TRANSPORTATION</h5>
					</div>
					<div class="sIcons">
						<img src="/wp-content/themes/KTBonesTheme/library/images/hourlyServiceIcon.png">
						<h5>DEDICATED HOURLY SERVICES</h5>
					</div>

				</div>

				<div class="flexDiv sCat2">
				<h2><?php the_field('homesec3_header'); ?></h2>
				<p><?php the_field('homesec3_copy'); ?></p>
				</div>

			</div>

	</div>

	<div class="greenButton">
			<a href="/services/" target="_self"><p id="white" >Learn More</h3></p></a>
		</div>
</div>
</section>

<!--
<section class="twoSec">
	<div class="twoCont">
		<div class="twoDiv"></div>
		<div class="twoDiv"></div>
	</div>
</section>

-->



</body>


<?php get_footer(); ?>
