<?php
/**
 Template Name: Fleet
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>
<head>
	<title>Services | AccuTrans Group</title>
	<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">

	<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
</head>

<body>



<section class="fleet">
	<div class="wrapper">
	<h1>Our Fleet</h1>
		<div class="flexSec cars">


				<div class="flexCont">
					<div class="flexDiv">
						<img src="/wp-content/themes/KTBonesTheme/library/images/AccuFleet_01.png" />
						<div class="carName">
							<h2>Sedan</h2>
							<p>
								<ul>
									<li>Up to 3 passengers</li>
									<li>Late model luxury sedans</li>
									<li>Black on black</li>
								</ul>
							</p>
						</div>
					</div>

					<div class="flexDiv cars">
						<img src="/wp-content/themes/KTBonesTheme/library/images/AccuFleet_02.png" />
						<div class="carName">
							<h2>Premium Sedan</h2>
							<p>
								<ul>
									<li>Late model BMW 7 series</li>
									<li>The epitome of class</li>
									<li>Black on black</li>
								</ul>
							</p>
						</div>
					</div>

					<div class="flexDiv cars">
						<img src="/wp-content/themes/KTBonesTheme/library/images/AccuFleet_03.png" />
						<div class="carName">
							<h2>SUV</h2>
							<p>
								<ul>
									<li>Fits up to 5 passengers</li>
									<li>Late model luxury SUV’s</li>
									<li>Black on black</li>
								</ul>
							</p>
						</div>
					</div>
				</div>

			<div class="flexSec cars">
			<div class="flexCont">
				<div class="flexDiv cars">
						<img src="/wp-content/themes/KTBonesTheme/library/images/AccuFleet_04.png" />
						<div class="carName">
							<h2>Stretch Limousines</h2>
							<p>
								<ul>
									<li>Up to 10 passengers</li>
									<li>Perfect fit for luxurious, small group transportation</li>
									<li>Plenty of luggage and passenger space</li>
								</ul>
							</p>
						</div>

					</div>

					<div class="flexDiv cars">
						<img src="/wp-content/themes/KTBonesTheme/library/images/AccuFleet_05.png" />
						<div class="carName">
							<h2>Stretch SUV</h2>
							<p>
								<ul>
									<li>Sizes vary from 4 to 14 passengers</li>
									<li>Make any special event more special!</li>
									<li>Black on black, classy, professional luxury transportation</li>
								</ul>

							</p>
						</div>
						<ul class="fleetInt">

						<li class="intLinks">
							<a href="/wp-content/themes/KTBonesTheme/library/images/limo_int1.jpg" class="magnific">interior 1</a>
						</li>
						<li class="intLinks">
							<a href="/wp-content/themes/KTBonesTheme/library/images/limo_int3.jpg" class="magnific">interior 2</a>
						</li>
						<li class="intLinks">
							<a href="/wp-content/themes/KTBonesTheme/library/images/limo_int2.jpg" class="magnific">interior 3</a>
						</li>

						</ul>
					</div>
				</div>
		</div>

		<div class="flexSec cars">
			<div class="flexCont">
				<div class="flexDiv cars">
						<img src="/wp-content/themes/KTBonesTheme/library/images/AccuFleet_06.png" />
						<div class="carName">
							<h2>Transit</h2>
							<p>
								<ul>
									<li>Up to 14 passengers</li>
									<li>Perfect fit for groups and airport transportation</li>
									<li>Late model black on black vans</li>
								</ul>
							</p>
						</div>
						<ul class="fleetInt">

						<li class="intLinks">
							<a href="/wp-content/themes/KTBonesTheme/library/images/transit_int1.jpg" class="magnific">interior 1</a>
						</li>
						<li class="intLinks">
							<a href="/wp-content/themes/KTBonesTheme/library/images/transit_int2.jpg" class="magnific">interior 2</a>
						</li>

						</ul>
					</div>
					<div class="flexDiv cars">
						<img src="/wp-content/themes/KTBonesTheme/library/images/AccuFleet_07.png" />
						<div class="carName">
							<h2>VanTerra</h2>
							<p>
								<ul>
									<li>Sizes vary from 4 to 14 passengers</li>
									<li>High-end wood flooring</li>
									<li>Individual leather wrapped captain’s chairs</li>
								</ul>

							</p>
						</div>

						<ul class="fleetInt">

						<li class="intLinks">
							<a href="/wp-content/themes/KTBonesTheme/library/images/vanterra_int1.jpg" class="magnific">interior 1</a>
						</li>
						<li class="intLinks">
							<a href="/wp-content/themes/KTBonesTheme/library/images/vanterra_int2.jpg" class="magnific">interior 2</a>
						</li>
						<li class="intLinks">
							<a href="/wp-content/themes/KTBonesTheme/library/images/vanterra_int3.jpg" class="magnific">interior 3</a>
						</li>

						</ul>


					</div>
				</div>
		</div>


		<div class="flexSec cars">
			<div class="flexCont">
				<div class="flexDiv cars">
						<img src="/wp-content/themes/KTBonesTheme/library/images/AccuFleet_08.png" />
						<div class="carName">
							<h2>Mini Coaches</h2>

								<ul>
									<li>Up to 33 passengers</li>
									<li>Excellent for group movements, outings, and airport transportation</li>
									<li>Forward facing executive style seating</li>
								</ul>


						</div>


						<ul class="fleetInt">
						<li class="intLinks">
							<a href="/wp-content/themes/KTBonesTheme/library/images/miniCoachInterior.jpeg" class="magnific">interior 1</a>
						</li>
						</ul>


					</div>
					<div class="flexDiv cars">
						<img src="/wp-content/themes/KTBonesTheme/library/images/AccuFleet_09.png" />
						<div class="carName">
							<h2>Motor Coaches</h2>
							<p>
								<ul>
									<li>Up to 55 passengers</li>
									<li>Excellent for group movements, outings, and any large capacity events</li>
								</ul>
							</p>
						</div>
					</div>
				</div>
		</div>
		</div>
	</div>
</section>





</body>


<?php get_footer(); ?>
