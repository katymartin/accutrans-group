<?php
/**
 Template Name: About
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>
<head>
	<title>Services | AccuTrans Group</title>
	<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">

	<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
</head>

<body>



<section class="about">
	<div class="wrapper">


	<h1><?php the_field('about_header'); ?></h1>
	<p><?php the_field('about_copy'); ?></p>



<div><h2 style="text-align: center; margin-top: 60px;">Grab and drag to explore our history</h2></div>

  <ul class="timeline dragscroll" id="timeline">


     <li class="li complete">
    <div class="timestamp">
     <img src="/wp-content/themes/KTBonesTheme/library/images/LimosInclogo.png">
      <p class="author">Limousines Incorporated is founded</p>
    </div>
    <div class="status">
      <h4>1982</h4>
    </div>
  </li>



   <li class="li complete">
    <div class="timestamp">
     <img src="/wp-content/themes/KTBonesTheme/library/images/oldLogo@2x.png">
      <p class="author">AccuTrans, Inc is founded</p>
    </div>
    <div class="status">
      <h4>2008</h4>
    </div>
  </li>



  <li class="li complete">
    <div class="timestamp">
     <img src="/wp-content/themes/KTBonesTheme/library/images/bus@2x.png">
      <p class="author">First Bus is acquired</p>
    </div>
    <div class="status">
      <h4>2009</h4>
    </div>
  </li>




  <li class="li complete">
    <div class="timestamp">
        <img src="/wp-content/themes/KTBonesTheme/library/images/globalTrans@2x.png">
      <p class="author">AccuTrans launches global car service bookings</p>
    </div>
    <div class="status">
      <h4>2010</h4>
    </div>
  </li>




   <li class="li complete">
    <div class="timestamp">
     <img src="/wp-content/themes/KTBonesTheme/library/images/merge@2x.png">
      <p class="author">AccuTrans and Limousines Inc merge making the perfect fleet variety</p>
    </div>
    <div class="status">
      <h4>2010</h4>
    </div>
  </li>
  <li class="li complete">
    <div class="timestamp">
     <img src="/wp-content/themes/KTBonesTheme/library/images/newBiz@2x.png">
      <p class="author">Small Business Administration “Emerging Business of the Year”</p>
    </div>
    <div class="status">
      <h4>2016</h4>
    </div>
  </li>
  <li class="li complete">
    <div class="timestamp">
     <img src="/wp-content/themes/KTBonesTheme/library/images/limo@2x.png">
      <p class="author">AccuTrans acquires Branko’s Limousine Service enhancing the fleet even more</p>
    </div>
    <div class="status">
      <h4>2016</h4>
    </div>
  </li>
  <li class="li complete">
    <div class="timestamp">
     <img src="/wp-content/themes/KTBonesTheme/library/images/vetAward@2x.png">
      <p class="author">One of Milwaukee Business Journal’s “Largest Veteran owned Business”</p>
    </div>
    <div class="status">
      <h4>2017</h4>
    </div>
  </li>
  <li class="li complete">
    <div class="timestamp">
     <img src="/wp-content/themes/KTBonesTheme/library/images/top50@2x.png">
      <p class="author">MMAC’s “Future 50” awardee</p>
    </div>
    <div class="status">
      <h4>2017</h4>
    </div>
  </li>
  <li class="li complete">
    <div class="timestamp">
     <img src="/wp-content/themes/KTBonesTheme/library/images/newLogo@2x.png">
      <p class="author">AccuTrans Group forms to better encompass all brands</p>
    </div>
    <div class="status">
      <h4>2017</h4>
    </div>
  </li>
 </ul>


	</div>
</section>






</body>


<?php get_footer(); ?>
